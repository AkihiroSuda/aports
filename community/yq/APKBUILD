# Contributor: Tuan Hoang <tmhoang@linux.ibm.com>
# Maintainer: Tuan Hoang <tmhoang@linux.ibm.com>
pkgname=yq
pkgver=4.12.2
pkgrel=0
pkgdesc="portable command-line YAML processor written in Go"
url="https://github.com/mikefarah/yq"
# riscv64 blocked by govendor
arch="all !riscv64"
license="MIT"
makedepends="go govendor"
options="chmod-clean"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikefarah/yq/archive/v$pkgver.tar.gz"

build() {
	GOPATH="$srcdir" go build -v
}

check() {
	GOPATH="$srcdir" go test
}

package() {
	install -Dm755 yq "$pkgdir"/usr/bin/yq
	for file in LICENSE README.md; do
		install -Dm644 $file "$pkgdir"/usr/share/doc/$pkgname/$file
	done

	mkdir -p "$pkgdir"/usr/share/bash-completion/completions \
			"$pkgdir"/usr/share/zsh/site-functions \
			"$pkgdir"/usr/share/fish/completions
	"$pkgdir"/usr/bin/yq shell-completion bash \
		> "$pkgdir"/usr/share/bash-completion/completions/yq.bash
	"$pkgdir"/usr/bin/yq shell-completion zsh \
		> "$pkgdir"/usr/share/zsh/site-functions/_yq
	"$pkgdir"/usr/bin/yq shell-completion fish \
		> "$pkgdir"/usr/share/fish/completions/yq.fish
}

sha512sums="
82571b5ab81440eef64230a1e1fb9f304702c3394aa5212a694d0f97aa300b6b7a2cbd67191ea0e06b567d8a003d2c6fe1b2727cbe72e04a3ff2db8e5dfdd5d1  yq-4.12.2.tar.gz
"
